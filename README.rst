Install MetadataManager (https://gitlab.esrf.fr/icat/tango-metadata)

.. code-block:: python

    git clone https://gitlab.esrf.fr/denolf/tango-metadata-installer.git
    cd tango-metadata-installer
    python -m pip install .
