from setuptools import setup, find_packages
import subprocess
import os
import re
import errno
import shutil

# Get the original project
ret = subprocess.call(['git',
                       'clone',
                       'git@gitlab.esrf.fr:denolf/tango-metadata.git',
                       'tango_metadata'])

# Extract name and version
project = None
version = None
projectpattern = re.compile(r"name=['\"]?([a-zA-Z]+)['\"]?")
versionpattern = re.compile(r"version=['\"]?([0-9\.]+)['\"]?")

for line in open(os.path.join('tango_metadata', 'setup.py')):
    if not project:
        for match in re.finditer(projectpattern, line):
            project = match.group(1)
    if not version:
        for match in re.finditer(versionpattern, line):
            version = match.group(1)

# Copy relevant part of the project
def copy(src, dest):
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)

src = os.path.join('tango_metadata', 'src', 'main', 'python', 'MetadataManager')
dest = os.path.abspath('MetadataManager')
copy(src, dest)

setup(
    name=project,
    version=version,
    packages=['MetadataManager'],
    install_requires=[],
    package_data={'MetadataManager': ['*.xmi', '*.xml']}
)
